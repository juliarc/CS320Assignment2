There were three programs and 2 differnt classes in this assignment 


Prog2_1: this was desinged to loop through an input by the user and deliminate by spaces. this was done by creating a start and and end variables that would locate the start and end of each word, and then a substring was saved into token1 and token2 while the number of tokens was counted. If there were no more than two tokens then the formats of the tokens were checked. if there was one token it should be a string, if there were two tokens it should be a string and an int. this program was run for N times, where N was a number that was put in through the user. it was compiled using g++ prog2_1.cpp

Tokenized.hpp: this was the header class for the tokenized.cpp

Tokenized.cpp: this #included the tokenized.hpp class. It then read in an input from the user and split it as it was done in program 1. howver if the input was two or one tokens in the correct format the tokens were stored in a vector of strings. Then the function returned a pointer to that vector. This program coudn't be compiled on its own because it does not have a main method

Prog2_2.cpp: this file used the tokenized.cpp to create an item of the token class. create N items of the token class, where N was a command line argument. All of these would return a correctly formated input becaue all of the errors were handled in Tokenized.cpp, the program would stop when reaching N inputs, or when the input was 'quit' This program was compiled with the Tokenized.cpp using g++ prog2_2.cpp Tokenized.cpp

Stack.hpp: was the header class for the stack class, this was a templated class so the header contained all of the templated methods.

Stack.cpp: this file included the header class but contained nothing else. 

Prog2_3.cpp: this program behaved like program 2 excpet it would check what the strings were.first a stack was created, then if the string for a one token was 'pop' then an item was poped. then if a two token was there and the first token was 'push' then the second token was pushed onto the stack. finaly the stack was printed once quiT was entered or there were N inputs. 

This was compiled with g++ prog2_3.cpp Stack.cpp Tokenized.cpp
