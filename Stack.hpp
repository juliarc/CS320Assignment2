#include<iostream>
#include<cstdlib>
#include <vector>
#include <stdlib.h>
#include<exception>

#define default_value 10

using namespace std;

template <class T>

class Stack {

private:

	T* data;
	int size;
	int last;

public:

	Stack(int = default_value);
	~Stack();
        void Push(T object);
	T Pop();
	void Print();

};


template <class T>
Stack<T>::Stack(int s) {

	this->data = (T*) malloc(sizeof(T) * s);
	this->size =s;
	this->last = -1;


}
template <class T>
Stack<T>::~Stack() {

        free(this->data);

}

template <class T>
void Stack<T>::Push(T input) {

        this->last++;
        this->data[this->last]= input;


}

//this method will pop off the last value and return it
template <class T>
T Stack<T>::Pop() {

        //check to make sure there is something in the stack
        try{
                T temp = this->data[this->last];
                this->last --;
                return temp;
        }
        catch(exception& e) {

                std::cout <<"Stack Empty!\n";

        }

}

//this will print all the objects from left to right 
template <class T>
void Stack<T>::Print() {

        std::cout <<"[";

        //loop through the stack
        for(int current = 0; current <= this->last; current++){

                std::cout << this->data[current]<< " ";
        }
        std::cout <<"]\n";



}

