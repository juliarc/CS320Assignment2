#include <stdio.h>
#include <iostream>
#include <string>
#include <sstream>
#include "Tokenizer.hpp"
#define TRUE 1
#define FALSE 0

using namespace std;


int is_int(string input) {




        //loop through and see if there are any non int charecters
        for(int i=0; i<input.size(); i++) {

                if(input.at(i) < '0' || input.at(i) > '9') {

                        return FALSE;

                }



        }
        return TRUE;
}


int  main(int argc, char* argv[]) {

        std::cout << "Assignment #2-2, Julia Cassella, juliarc@sandiego.edu\n";

       

	 //check to see if there is the correct number of command line args
        if(argc != 2) {

                std::cout << "ERROR! Program 1 accepts 1 command line argument.\n";
                return 0;
        }

        std::string argument = argv[1];

        //check to see if the command line arg is an integer
	if(is_int(argument) == FALSE) {

		std::cout << "ERROR! Expected Integer Argument\n";
		return 0;
	}



	//at this point there should be the correct number of args and it should be an integer 
        istringstream buffer(argument);
        int N ;
        buffer >> N;

        //now prompt for N inputs from the user 
        int prompt = 0;

        while (prompt < N) {

	//define an object of the Tokenizer class
        Tokenizer t1;

		//make a pointer that will point to our current vector
		std::vector<std::string>* vec;
		vec =t1.GetTokens();


		//Now handel when the legnth of the vector is 1
		if(vec->size() ==1) {


			std::string token1 = vec->back();
			//check to see if string is quit
			if(token1 == "quiT") {
				
				return 0;
			}
	
			else
				std::cout << "STR\n";

		
		}
		else {

			std::cout << "STR INT\n";
	

		}		
		prompt++;
		vec->clear();
	}


}




