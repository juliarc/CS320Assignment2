#include "Tokenizer.hpp" 
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#define FALSE  0
#define TRUE 1

using namespace std;

std::vector<std::string>* Tokenizer:: GetTokens(){

	static std::vector<std::string> tokanized;

	int correct =0;

	std::string token1;
        std::string token2;
	int num_token;
	std::string input;	

//do this until the correct input is put in
while(correct == FALSE) {

	//read in from the user 
	std::cout << "> ";
	
	std::string input;
        getline (cin,input);

	tokanized.clear();
        num_token = 0;
       

        //scan the input and split at the white spaces 

        int start = 0;
        int end = 0;

         //remove any leading 0's 
        while(start < input.size() && input.at(start) == ' '){

                start ++;


        }
        end = start;

         //remove any tailing 0's 

        int max = input.size() -1;

        while( max > 0 && (input.at(max) == ' ')) {
                max--;

        }
        //loop through and find the deliminated sections 
        //check to see if the input was only one charecter 

        if(max==0 && input.at(max) != '\0') {

                num_token++;
                token1 = input;

        }

	 while(end < max) {

                if(input.at(end) == ' ' || (end == (max-1 ))) {
                        int leng = end-start;


                        num_token+=1;

                        //if we are looking at the first token set it
                        if(num_token == 1) {

                                //if the end was a space then we can copy from start with length leng
                                if(input.at(end) == ' ' ) {

                                        token1 = input.substr(start,leng);

                                }

                                //else you have to do leng  + 1
                                else
                                        token1= input.substr(start, (leng+2));
                        }

                        //set the second token
                        if(num_token ==2) {

                                 //if the end was a space then we can copy from start with length leng
                                if(input.at(end) == ' ' ) {

                                        token2 = input.substr(start,leng);

                                }

                                //else you have to do leng  + 1
                                else
                                        token2= input.substr(start, (leng+2));
                        }
                        if(end == (max-1)) {

                                end++;

                        }

                        while(input.at(end) == ' ' && end<max) {
                               end++;
                               start = end;
                        }


                }

                else
                        end++;
        }
	//now check if there is just one thing by itself at the end
        if(end != 0 && input.at(end) != ' ' && input.at(end-1) == ' ') {

                num_token++;
                if(num_token == 2) {

                        token2 = input.at(end);

                }

        }
        //at this point both of the tokens should be set and there should be a token count 

        //if there is one token check to make sure it is the correct input 
        if(num_token == 1 || num_token ==2) {

               if(num_token == 1) {
			//see if the token is a string
			
			for(int i=0; i<token1.size(); i++) {

				if(token1.at(i) < '0' || token1.at(i) > '9') {
					//then we have a string
					correct = TRUE;
				}
			}
			
			//then we must have an int
			if(correct == FALSE) {
				std::cout << "ERROR! Expected STR.\n";
			}
		
		}

		else {

			int first_string = FALSE;
			int second_string = FALSE;

			//loop through token 1 and see if it is a string
			 for(int i=0; i<token1.size(); i++) {

                                if(token1.at(i) < '0' || token1.at(i) > '9') {
                                        //then we have a string
                                        first_string = TRUE;
                                }
                        }


			//loop through token 2 and see if it is an int
			 for(int i=0; i<token2.size(); i++) {

                                if(token2.at(i) < '0' || token2.at(i) > '9') {
                                        //then we have a string
                                       second_string = TRUE;
                                }
                        }

                        //check to see if the first_string is true and second_string is false
			if(first_string == TRUE && second_string == FALSE) {

				correct = TRUE;			

			}

			else 

				std::cout << "ERROR! Expected STR INT.\n";


		}


		
	}
        else {

                std::cout << "ERROR! Incorrect number of tokens found.\n";
        }


}

	//so there are either one or two tokens now
	


	if(num_token ==1) {
	
		tokanized.push_back(token1);	
	}

	else {
	
		tokanized.push_back(token1);
		tokanized.push_back(token2);
	}
	return &tokanized;
}
