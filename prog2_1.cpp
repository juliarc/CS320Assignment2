#include <stdio.h>
#include <iostream>
#include <string>
#include <sstream>
#define TRUE 1
#define FALSE 0

using namespace std;


int is_int(string input) {




	//loop through and see if there are any non int charecters
	for(int i=0; i<input.size(); i++) {

		if(input.at(i) < '0' || input.at(i) > '9') {

			return FALSE;

		}

	

	}
	return TRUE;
}

void tokenize(string input) {

	int num_token = 0;
	std::string token1;
	std::string token2;
	
	//scan the input and split at the white spaces 

	int start = 0;
        int end = 0;

         //remove any leading 0's 
        while(start < input.size() && input.at(start) == ' '){

                start ++;


        }
        end = start;

         //remove any tailing 0's 

        int max = input.size() -1;

        while( max > 0 && (input.at(max) == ' ')) {
                max--;

        }

        //loop through and find the deliminated sections 

	//check to see if the input was only one charecter 

	if(max==0 && input.at(max) != '\0') {

		num_token++;
		token1 = input;

	}

        while(end < max) {

                if(input.at(end) == ' ' || (end == (max-1 ))) {
                        int leng = end-start;
                        

                        num_token++;

			//if we are looking at the first token set it
			if(num_token == 1) {
				
				//if the end was a space then we can copy from start with length leng
				if(input.at(end) == ' ' ) {
					
					token1 = input.substr(start,leng);
		
				}

				//else you have to do leng  + 1
				else 
					token1= input.substr(start, (leng+2));


			}

			//set the second token
			if(num_token ==2) {

				 //if the end was a space then we can copy from start with length leng
                                if(input.at(end) == ' ' ) {

                                        token2 = input.substr(start,leng);

                                }

                                //else you have to do leng  + 1
                                else
                                        token2= input.substr(start, (leng+2));


			}

                        if(end == (max-1)) {

                                end++;

                        }

                        while(input.at(end) == ' ' && end<max) {
                               end++;
                               start = end;
                        }


                }

                else
                        end++;
        }

	//now check if there is just one thing by itself at the end
	if(end != 0 && input.at(end) != ' ' && input.at(end-1) == ' ') {
	
		num_token++;
		if(num_token == 2) {
		
			token2 = input.at(end);

		}	

	}	



	//at this point both of the tokens should be set and there should be a token count 

	//if there is one token check to make sure it is the correct input 
	if(num_token == 1) {

		if(is_int(token1) == FALSE) {

			std::cout << "STR\n";

		}

		else {

			std::cout << "ERROR! Expected STR.\n";

		}

	}

	else if(num_token ==2) {

		if(is_int(token1) == FALSE && is_int(token2) == TRUE) {

			std::cout <<"STR INT\n";

		}

		else 

			std::cout <<"ERROR! Expected STR INT.\n";
		

	}

	else {

		std::cout << "ERROR! Incorrect number of tokens found.\n";

	}

        


}
int  main(int argc, char* argv[]) {

	std::cout << "Assignment #2-1, Julia Cassella, juliarc@sandiego.edu\n";

	//check to see if there is the correct number of command line args
	if(argc != 2) {
	
		std::cout << "ERROR! Program 1 accepts 1 command line argument.\n";
		return 0;
	}

	std::string argument = argv[1];

	//check to see if the command line arg is an integer
	for(int i = 0; i < argument.size(); i++) {

		if( argument.at(i) < '0' || argument.at(i) > '9') {

			std::cout << "ERROR! Expected Integer Argument\n";
			return 0;
		
		}
	}


	//at this point there should be the correct number of args and it should be an integer 
	istringstream buffer(argument);
	int N ;
	buffer >> N;

	//now prompt for N inputs from the user 
	int prompt = 0;

	while (prompt < N) {

		std::cout << "> ";
		string input;
		getline (cin,input);

		//check to see if it is equal to quit 
		if(input == "quiT") {
			return 0;
		}	


		//pass it into the tokanization function which will tokanize and check output
		tokenize(input);	
		prompt++;
	}
	
}






